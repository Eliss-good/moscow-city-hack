"""empty message

Revision ID: dc7444888d26
Revises: 
Create Date: 2022-06-12 20:21:35.036304

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'dc7444888d26'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('categories',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('community',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('community_name', sa.String(), nullable=True),
    sa.Column('community_description', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('coordinates_event',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('address', sa.String(), nullable=False),
    sa.Column('coordinates', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('tags',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('tag_name', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('users',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('user_name', sa.String(), nullable=True),
    sa.Column('login', sa.String(), nullable=False),
    sa.Column('password', sa.Unicode(), nullable=False),
    sa.Column('role', sa.Integer(), nullable=True),
    sa.Column('email', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('login')
    )
    op.create_table('click_history',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('tags_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['tags_id'], ['tags.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('customers',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('coordinates', sa.String(), nullable=True),
    sa.Column('points', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('community_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['community_id'], ['community.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('employer',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('discription', sa.String(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('tags_categories',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('categories_id', sa.Integer(), nullable=True),
    sa.Column('tags_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['categories_id'], ['categories.id'], ),
    sa.ForeignKeyConstraint(['tags_id'], ['tags.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('tags_community',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('tags_id', sa.Integer(), nullable=True),
    sa.Column('community_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['community_id'], ['community.id'], ),
    sa.ForeignKeyConstraint(['tags_id'], ['tags.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('tags_users',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('tags_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['tags_id'], ['tags.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('advertisement',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('adv_name', sa.String(), nullable=True),
    sa.Column('time_event', sa.Time(), nullable=True),
    sa.Column('type_event', sa.String(), nullable=True),
    sa.Column('time_create', sa.DateTime(), nullable=True),
    sa.Column('wait_size_users', sa.Integer(), nullable=True),
    sa.Column('employer_id', sa.Integer(), nullable=True),
    sa.Column('coordinates_id', sa.Integer(), nullable=True),
    sa.Column('categories_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['categories_id'], ['categories.id'], ),
    sa.ForeignKeyConstraint(['coordinates_id'], ['coordinates_event.id'], ),
    sa.ForeignKeyConstraint(['employer_id'], ['employer.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('approved_customer',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('status', sa.Boolean(), nullable=True),
    sa.Column('adv_id', sa.Integer(), nullable=True),
    sa.Column('customer_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['adv_id'], ['advertisement.id'], ),
    sa.ForeignKeyConstraint(['customer_id'], ['customers.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('community_adv',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('community_id', sa.Integer(), nullable=True),
    sa.Column('adv_id', sa.Integer(), nullable=True),
    sa.Column('is_repost', sa.Boolean(), nullable=True),
    sa.Column('reposted_by', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['adv_id'], ['advertisement.id'], ),
    sa.ForeignKeyConstraint(['community_id'], ['community.id'], ),
    sa.ForeignKeyConstraint(['reposted_by'], ['customers.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('tags_advertisement',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('adv_id', sa.Integer(), nullable=True),
    sa.Column('tags_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['adv_id'], ['advertisement.id'], ),
    sa.ForeignKeyConstraint(['tags_id'], ['tags.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('tags_advertisement')
    op.drop_table('community_adv')
    op.drop_table('approved_customer')
    op.drop_table('advertisement')
    op.drop_table('tags_users')
    op.drop_table('tags_community')
    op.drop_table('tags_categories')
    op.drop_table('employer')
    op.drop_table('customers')
    op.drop_table('click_history')
    op.drop_table('users')
    op.drop_table('tags')
    op.drop_table('coordinates_event')
    op.drop_table('community')
    op.drop_table('categories')
    # ### end Alembic commands ###
