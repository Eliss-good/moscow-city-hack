from web_setting import flask_app, ma
from models.all_models import Customers, Users, TagsUsers, Tags

from api.api_users import UsersSchema, _base_query_user_tag


class CustomersSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Customers
        load_instance = True
        include_relationships = True
        exclude = ['customer_approve']

    connect_users = ma.Nested(UsersSchema(exclude=('users_click','users_emp', 'users_cus', 'password','id', )))


def _base_query():
    return Customers.query

def all_customers():
    query_data = _base_query()
    api_customers = CustomersSchema(many=True)

    return api_customers.dump(query_data)


def filter_customers(login=None,
                     tags: list = None,
                     user_id: list = []):
    query_data = _base_query()
    api_customers = CustomersSchema(many=True)

    if tags is not None:
        print(_base_query_user_tag().all())

        query_tags_us = _base_query_user_tag()
        query_tags_us = query_tags_us.distinct(TagsUsers.user_id)
        query_tags_us = query_tags_us.filter(Tags.tag_name.in_(tags)).all()

        user_id = []
        for element in query_tags_us:
            users_ids.append(element[0])

    if user_id is not None:
        query_data = query_data.filter(Customers.user_id.in_(user_id))

    if login is not None:
        query_data = query_data.join(Users)
        query_data = query_data.filter(Users.login == login)

    api_customers = CustomersSchema(many=True)
    return api_customers.dump(query_data)