from web_setting import db, flask_app, get_jwt, verify_jwt_in_request, create_access_token, cross_origin
from functools import wraps

from flask import jsonify, request
from werkzeug.security import generate_password_hash, check_password_hash

from routers.all_dataclasses import EntryDate

from models.all_models import Users


def admin_required():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            verify_jwt_in_request()
            claims = get_jwt()
            if claims["is_administrator"]:
                return fn(*args, **kwargs)
            else:
                return jsonify(msg="Admins only!"), 403

        return decorator
    return wrapper


def login(Entry: EntryDate):
    username = Entry.login
    password = Entry.password

    query_user = db.session.query(Users)
    query_user = query_user.filter(Users.login == username).first()

    if query_user != None:
        if check_password_hash(query_user.password, password):
            access_token = create_access_token(
                "admin_user", additional_claims={"is_administrator": True}
            )
            return {'access_token':access_token,
                    'user_id':query_user.id,
                    'role': query_user.role}
    else:
        return {"msg": "Bad username or password"}
