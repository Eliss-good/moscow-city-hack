from web_setting import flask_app, ma, db
from models.all_models import Community, CommunityAdv

#from api.api_tags import TagsSchema
from api.api_employer import EmployerSchema
from api.api_categories import CategoriesSchema
from api.api_customers import CustomersSchema
from api.api_tags import TagsGlobalSchema
from api.api_advertisment import AdvertisementSchema


class CoommunityAdvSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CommunityAdv
        load_instance = True
        include_relationships = True

    connect_reposted_by = ma.Nested(CustomersSchema(exclude=('comm_adv_reposted_by_customers', )))
    connect_adv_id = ma.Nested(AdvertisementSchema(only=('connect_coordinates', 'adv_id_tagsadv', 'time_create',
                                                         'wait_size_users', 'id', 'adv_name', )))


class CommunitySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Community
        load_instance = True
        include_relationships = True
        exclude = ['tags_community_community']

    comm_adv_comm_id_community = ma.List(ma.Nested(CoommunityAdvSchema(exclude=('connect_community_id',) )))
    community_cus = ma.List(ma.Nested(CustomersSchema()))
    connect_customer = ma.Nested(CustomersSchema(exclude=( 'customer_approve', 'id',)))


def _base_query():
    return db.session.query(Community)


def all_communities():
    data_query = _base_query()
    api_all_community = CommunitySchema(many=True)
    return api_all_community.dump(data_query)


def filter_communities(community_id=None):
    data_query = _base_query()

    if community_id is not None:
        data_query = data_query.filter(Community.id == community_id)

    api_all_adv = CommunitySchema(many=True)
    return api_all_adv.dump(data_query)
