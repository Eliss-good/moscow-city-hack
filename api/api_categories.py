from web_setting import flask_app, ma, db
from models.all_models import Categories

class CategoriesSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Categories
        load_instance = True
        include_relationships = True