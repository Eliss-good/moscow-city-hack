from web_setting import flask_app, ma, db
from models.all_models import Advertisement, ApprovedCustomer, СoordinatesEvent, TagsAdvertisement

#from api.api_tags import TagsSchema
from api.api_employer import EmployerSchema
from api.api_categories import CategoriesSchema
from api.api_customers import CustomersSchema
from api.api_tags import TagsGlobalSchema


class ApprovedCustomerSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ApprovedCustomer
        load_instance = True
        include_relationships = True
        exclude = ['connect_adv']

    connect_customer = ma.Nested(CustomersSchema(exclude=( 'customer_approve', 'id', )))


class CoordinateSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = СoordinatesEvent
        load_instance = True
        include_relationships = True


class TagsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = TagsAdvertisement
        load_instance = True
        include_relationships = True

    connect_tags = ma.Nested(TagsGlobalSchema(only=['tag_name', 'id']))


class AdvertisementSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Advertisement
        load_instance = True
        include_relationships = True

    connect_employer = ma.Nested(EmployerSchema(exclude=('advertisement_employer','id',)))
    adv_id_approvedcustomer = ma.List(ma.Nested(ApprovedCustomerSchema()))
    connect_categories = ma.Nested(CategoriesSchema(exclude=('categories_advertisement','id',)))
    adv_id_tagsadv = ma.List(ma.Nested(TagsSchema(only=['connect_tags'])))
    connect_coordinates = ma.Nested(CoordinateSchema(exclude=('categories_coordinates','id', )))


def _base_query():
    return db.session.query(Advertisement)


def all_advertisments():
    data_query = _base_query()
    api_all_adv = AdvertisementSchema(many=True)
    return api_all_adv.dump(data_query)


def filter_advertisments(id_adv=None):
    data_query = _base_query()

    if id_adv is not None:
        data_query = data_query.filter(Advertisement.id == id_adv)

    api_all_adv = AdvertisementSchema(many=True)
    return api_all_adv.dump(data_query)


def indexes_advertisments(indexes: list = None):
    data_query = _base_query()

    if indexes:
        in_expression = Advertisement.id.in_(indexes)
        data_query.filter(in_expression)

    api_all_adv = AdvertisementSchema(many=True)
    return api_all_adv.dump(data_query)



