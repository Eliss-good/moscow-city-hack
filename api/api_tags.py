from web_setting import flask_app, ma, db
from models.all_models import Tags


class TagsGlobalSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Tags
        load_instance = True
        include_relationships = True

