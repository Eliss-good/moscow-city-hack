from web_setting import flask_app, ma
from models.all_models import Employer, Users

from api.api_users import UsersSchema


class EmployerSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Employer
        load_instance = True
        include_relationships = True

    connect_users = ma.Nested(UsersSchema(exclude=('users_cus','users_emp','password',)))
