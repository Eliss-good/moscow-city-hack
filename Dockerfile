FROM python:latest

WORKDIR .

COPY req.txt req.txt
RUN pip3 install -r req.txt

COPY . ./

CMD [ "python3", "./main.py"]