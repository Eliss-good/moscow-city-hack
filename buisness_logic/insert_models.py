from web_setting import flask_app, db
from models.all_models import Advertisement, Users, Employer, Customers, TagsUsers, ClickHistory, CommunityAdv, CustomersCommunity
from werkzeug.security import generate_password_hash, check_password_hash

from routers.all_dataclasses import EntryDate, TagsIndex, RepostEvent, CustCommunity
from typing import List


def add_new_users(RegisterData: EntryDate):
    hash_password = generate_password_hash(RegisterData.password)
    new_users = Users(
        new_name=RegisterData.user_name,
        new_login=RegisterData.login,
        role=RegisterData.role,
        password=hash_password,
        new_email=RegisterData.email
    )
    db.session.add(new_users)
    db.session.commit()

    if RegisterData.role == 0:
        new_con = Employer(user_id=new_users.id, new_discription=RegisterData.discription)
        db.session.add(new_con)
        db.session.commit()

    elif RegisterData.role == 1:
        new_con = Customers(user_id=new_users.id, coords=RegisterData.coordinat)
        db.session.add(new_con)
        db.session.commit()

    return "successful registration"


def add_new_repost(new_repost: RepostEvent):
    repost = CommunityAdv(new_community_id=new_repost.community_id_id,
                          new_reposted_by=new_repost.custimers_id,
                          new_adv_id=new_repost.adv_id,
                          new_is_repost=True)

    db.session.add(repost)
    db.session.commit()


def add_new_click(NewClick: TagsIndex):
    clicks_queue = []

    for one_click in NewClick.tags_id:
        new_click = ClickHistory(user_id=NewClick.index_connect, tags_id=one_click)
        clicks_queue.append(new_click)

    db.session.add_all(clicks_queue)
    db.session.commit()


def add_new_connect_community(new_connect: CustCommunity):
    comm_customers = CustomersCommunity(customers_id=new_connect.customers_id, community_id=new_connect.community_id)
    db.session.add(comm_customers)
    db.session.commit()


def create_advertisement(adv_info):
    new_advertisement = Advertisement(
        new_adv_name=adv_info.adv_name,
        new_time_event=adv_info.time_event,
        new_type_event=adv_info.type_event,
        new_employer_id=adv_info.employer_id,
        new_coordinates_id=adv_info.coordinates_id,
        new_categories_id=adv_info.categories_id,
        new_wait_size_users=adv_info.wait_size_users
    )

    db.session.add(new_advertisement)
    db.session.commit()

    return 'advert add success'



""" 1) Подписка на комьюнити и инскрт новых коммьюнити
    2) Создание новых объявлений
    3) обработка друзей и лоби"""
