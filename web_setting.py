import sys
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_cors import CORS, cross_origin

from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt
from flask_jwt_extended import JWTManager
from flask_jwt_extended import verify_jwt_in_request
from flask_jwt_extended import jwt_required

basedir = os.path.abspath(os.path.dirname(__file__))
flask_app = Flask(__name__)
CORS(flask_app, supports_credentials=True)

flask_app.config['CORS_HEADERS'] = 'application/json'
flask_app.config['FLASK_ENV'] = 'development'
flask_app.config['SECRET_KEY'] = 'pop'
flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#  DATABASE SETTINGS
flask_app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://oleg101:a,qG!W-crt3,Rdy@rc1a-zdkc1zvqf7c2ma7i.mdb.yandexcloud.net:6432/db1'

#flask_app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://db_agent:a,qG!W-crt3,Rdy@rc1a-tmis74wmp0zyh2tl.mdb.yandexcloud.net:6432/db1'
# flask_app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://admin:admin@localhost/voland'

flask_app.config['JWT_SECRET_KEY'] = 'key-eliseev-gay'
jwt = JWTManager(flask_app)

db = SQLAlchemy(flask_app)
ma = Marshmallow(flask_app)
admin = Admin(flask_app)
