from api.api_community import filter_communities
from buisness_logic.insert_models import add_new_repost
from web_setting import flask_app, cross_origin
from api.api_community import all_communities

from routers.all_dataclasses import RepostEvent
from flask import jsonify, request


@flask_app.route('/api/communities', methods=["GET"])
def communities():
    result = all_communities()
    return jsonify(result)


@flask_app.route('/api/communities/repost', methods=["POST"])
def repost():
    new_repost = RepostEvent(**request.get_json())

    add_new_repost(new_repost)
    return jsonify('supper')


@flask_app.route('/api/tzt', methods=["GET"])
def sg():
    result = filter_communities(2)
    return jsonify(result)
