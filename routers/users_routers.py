from web_setting import flask_app, cross_origin
from api.api_users import all_users, filter_users
from api.api_customers import all_customers, filter_customers

from buisness_logic.insert_models import add_new_connect_community
from routers.all_dataclasses import CustCommunity

from pydantic import BaseModel, constr
from typing import Optional
from flask import jsonify, request


@flask_app.route('/api/users/', methods=["GET"])
@flask_app.route('/api/users/<int:user_id>', methods=["GET"])
def users(user_id=None):
    if user_id is not None:
        result = filter_users(user_id)

    result = all_users()
    return jsonify(result)


@flask_app.route('/api/customers', methods=["GET"])
def customers():
    result = all_customers()
    return jsonify(result)


@flask_app.route('/api/customers/community', methods=["POST"])
def customers_community():
    res = CustCommunity(**request.get_json())
    if res.status == 'delete':
        delete_con_community(res)
    elif res.status == 'insert':
        add_new_connect_community(res)

    return jsonify('sex')
    #return jsonify(filter_users(res))


@flask_app.route('/api/customers/filter/login/<string:login>/', methods=["GET"])
@flask_app.route('/api/customers/filter/user_id/<int:user_id>/', methods=["GET"])
def cust_filter(login=None, user_id=None):
    result = filter_customers(login=login, user_id=[user_id])
    return jsonify(result)


@flask_app.route('/api/customers/filter/tags/', methods=["POST"])
def cust_filter_tag():
    result = filter_customers(tags=request.json.get('tags', None))
    return jsonify(result)