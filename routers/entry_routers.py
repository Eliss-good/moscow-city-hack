from web_setting import flask_app, cross_origin
from api.api_auth import login
from buisness_logic.insert_models import add_new_users

from routers.all_dataclasses import EntryDate

from flask import jsonify, request


@flask_app.route('/register', methods=['POST'])
@cross_origin(origins=["*"], supports_credentials=True)
def register_user():
    reg_data = EntryDate(**request.get_json())
    return jsonify(add_new_users(reg_data))


@flask_app.route('/login', methods=['POST'])
@cross_origin(origins=["*"], supports_credentials=True)
def login_user():
    """Возвращает JWT токен"""
    login_data = EntryDate(**request.get_json())
    return jsonify(login(login_data))


        




