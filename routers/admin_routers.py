from web_setting import ModelView, db, admin
from models.all_models import *


class UsersView(ModelView):
    form_columns = ['user_name', 'login', 'password', 'role', 'email']


class TagView(ModelView):
    form_columns = ['tag_name']


class EmployersView(ModelView):
    form_columns = ['connect_users', 'discription']


class CustomerView(ModelView):
    form_columns = ['connect_users', 'coordinates', 'points']


class CoordinatEventView(ModelView):
    form_columns = ['address', 'coordinates']


class AdvertisementView(ModelView):
    form_excluded_columns = ['adv_id_approvedcustomer', 'adv_id_tagsadv', 'advertisement_community']


admin.add_view(UsersView(Users, db.session))
admin.add_view(TagView(Tags, db.session))
admin.add_view(EmployersView(Employer, db.session))
admin.add_view(ModelView(Categories, db.session))
admin.add_view(ModelView(Community, db.session))
admin.add_view(CustomerView(Customers, db.session))
admin.add_view(CoordinatEventView(СoordinatesEvent, db.session))
admin.add_view(AdvertisementView(Advertisement, db.session))
admin.add_view(ModelView(ApprovedCustomer, db.session))
admin.add_view(ModelView(CommunityAdv, db.session))

admin.add_view(ModelView(ClickHistory, db.session))
admin.add_view(ModelView(TagsCommunity, db.session))
admin.add_view(ModelView(TagsCategories, db.session))
admin.add_view(ModelView(TagsUsers, db.session))
admin.add_view(ModelView(TagsAdvertisement, db.session))