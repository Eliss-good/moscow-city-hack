import numpy as np
from scipy import sparse
from sklearn.preprocessing import normalize
from random import randint
from web_setting import db
from models.all_models import ClickHistory
from collections import Counter
from api.api_advertisment import all_advertisments
from geopy.distance import geodesic


def get_adv_in_radius(user_location_str: str, radius: int):
    user_location = user_location_str.split(', ')
    user_location = (float(user_location[0]), float(user_location[1]))
    all_adv = all_advertisments()
    result_id = []
    for _ in all_adv:
        coords = _['connect_coordinates']['coordinates'].split(', ')
        coords = (float(coords[0]), float(coords[1]))
        if geodesic(user_location, coords).km <= radius:
            result_id.append(_['id'])
    return result_id

# print(get_adv_in_radius('55.53570015221644, 36.95149256704093', 1))

def calc_recomendation_by_tags(user_data, all_data):
    row_column_index = []
    for i in range(len(user_data)):
        row_column_index.append(randint(0, 10))

    rows, rows_position = np.unique(row_column_index, return_inverse=True)
    cols, columns_position = np.unique(list(user_data.keys()), return_inverse=True)

    interaction_Sparse = sparse.csr_matrix((list(user_data.values()), (rows_position, columns_position)),
                                           shape=(len(all_data), len(all_data)))
    normalized_matrix = normalize(interaction_Sparse, norm="l2", axis=1)
    symmertic_matrix = normalized_matrix.T * normalized_matrix
    symmertic_matrix.todense()
    result = [all_data[i + 1] for i in symmertic_matrix[0].toarray().argsort()[0]]

    interaction_Sparse_transpose = interaction_Sparse.transpose(copy=True)
    normalized_matrix_transpose = normalize(interaction_Sparse_transpose, norm="l2", axis=1)

    symmertic_matrix_transpose = normalized_matrix * normalized_matrix_transpose * normalized_matrix

    result = [all_data[i + 1] for i in symmertic_matrix_transpose[0].toarray().argsort()[0]]

    result_id = []
    for i in range(len(result)):
        result_id.append(list(all_data.keys())[list(all_data.values()).index(result[i])])

    for x in result_id:
        if result_id.count(x) > 1:
            result_id.remove(x)
            for index in list(all_data.keys()):
                if index not in result_id:
                    result_id.insert(x, index)

    return result_id


def calc_recomendation_by_location(user_data, all_data, tags_recomendation):

    row_column_index = []
    for i in range(len(tags_recomendation)):
        row_column_index.append(randint(0, 10))

    destination = []
    for i in range(len(all_data)):
        destination.append(((list(all_data.values())[i][0] - list(user_data.values())[0][0]) ** 2 +
                            (list(all_data.values())[i][1] - list(user_data.values())[0][1]) ** 2) ** 0.5)

    priority = []
    for i in range(len(tags_recomendation)):
        priority.append(i / len(tags_recomendation))

    priority.sort(reverse=True)
    rows, rows_position = np.unique(row_column_index, return_inverse=True)
    cols, columns_position = np.unique(priority, return_inverse=True)

    interaction_Sparse = sparse.csr_matrix((tags_recomendation, (rows_position, columns_position)),
                                           shape=(len(destination), len(destination)))

    normalized_matrix = normalize(interaction_Sparse, norm="l2", axis=1)
    symmertic_matrix = normalized_matrix.T * normalized_matrix
    symmertic_matrix.todense()

    keys = list(all_data.keys())
    for key, i in zip(keys, range(len(all_data))):
        all_data[key] = destination[i]
    result = [all_data[i + 1] for i in symmertic_matrix[0].toarray().argsort()[0]]
    interaction_Sparse_transpose = interaction_Sparse.transpose(copy=True)
    normalized_matrix_transpose = normalize(interaction_Sparse_transpose, norm="l2", axis=1)

    symmertic_matrix_transpose = normalized_matrix * normalized_matrix_transpose * normalized_matrix

    result = [all_data[i + 1] for i in symmertic_matrix_transpose[0].toarray().argsort()[0]]

    result_id = []
    for i in range(len(result)):
        result_id.append(list(all_data.keys())[list(all_data.values()).index(result[i])])

    for x in result_id:
        if result_id.count(x) > 1:
            result_id.remove(x)
            for index in list(all_data.keys()):
                if index not in result_id:
                    result_id.insert(x, index)

    return result_id


def get_recomendation_by_tags(user_id: int, all_adv=all_advertisments()):
    queryset = db.session.query(ClickHistory).filter(ClickHistory.user_id==user_id).all()
    print(queryset)
    all_tags = []
    for _ in queryset:
        all_tags.append(_.tags_id)

    all_tags.sort()

    user_data = dict(Counter(all_tags))

    all_data = dict()

    for _ in all_adv:
        tag_id = []
        for val in _['adv_id_tagsadv']:
            tag_id.append(val['connect_tags']['id'])
        all_data[_['id']] = tag_id

    result_id = calc_recomendation_by_tags(user_data, all_data)
    return result_id


def get_recomendation_by_location(user_id: int, location_str: str):
    location = location_str.split(', ')
    location[0], location[1] = float(location[0]), float(location[1])
    distance_user = {user_id: location}
    distance_data = dict()
    all_adv = all_advertisments()
    for _ in all_adv:
        coords = _['connect_coordinates']['coordinates'].split(', ')
        coords[0], coords[1] = float(coords[0]), float(coords[1])
        distance_data[_['id']] = coords

    return calc_recomendation_by_location(distance_user, distance_data, get_recomendation_by_tags(user_id, all_adv))